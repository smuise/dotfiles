#!/usr/bin/env python

import json
from datetime import datetime, timedelta
import time
from os.path import expanduser
import sys

# Hide any exceptions
def exception_handler(exception_type, exception, traceback):
    print

sys.excepthook = exception_handler

json_filepath = expanduser("~") + "/.cache/spacex.json"

with open(json_filepath, "r+") as json_file:
    spacex_launch = json.loads(json_file.read())

now = datetime.now()
delta = timedelta(hours=12)
launch_time = datetime.fromtimestamp(spacex_launch['date_unix'])
precision = spacex_launch['date_precision']

t_minus = launch_time - now

if precision == 'hour' and t_minus <= delta and launch_time >= now:
    t_minus_display = str(t_minus)[:-10]
    mission = spacex_launch['name']
    print("T-{} - {} ".format(t_minus_display, mission))
