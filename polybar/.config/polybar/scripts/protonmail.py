#!/usr/bin/python

import os
import sys

def exception_handler(exception_type, exception, traceback):
    print

sys.excepthook = exception_handler

netrc = '/home/smuise/.config/polybar/scripts/.pmb'
server = '127.0.0.1:1143'


inbox = os.popen('curl -sf --netrc-file "{}" -X "STATUS INBOX (UNSEEN)" imap://"{}"| tr -d -c "[:digit:]"'.format(netrc, server)).read()

if int(inbox) > 0:
    inboxCount = inbox
else:
    inboxCount = ''

print(inboxCount)
