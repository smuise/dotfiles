#!/usr/bin/python

import os
import sys

def exception_handler(exception_type, exception, traceback):
    print

sys.excepthook = exception_handler

netrc = '/home/smuise/.config/polybar/scripts/.pmb'
server = '127.0.0.1:1143'


lists = os.popen('curl -sf --netrc-file "{}" -X \'STATUS "Folders/Mailing Lists" (UNSEEN)\' imap://"{}"| tr -d -c "[:digit:]"'.format(netrc, server)).read()

if int(lists) > 0:
    listsCount = lists
else:
    listsCount = ''

print(listsCount)
