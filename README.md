# Dotfiles

## Usage

Pull the repo & use GNU stow to create the necessary symlinks

```
git clone https://gitlab.com/smuise/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
stow bspwm sxhkd ... # list any other folders you want to import
```
