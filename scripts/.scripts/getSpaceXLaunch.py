#!/usr/bin/env python

# This script writes the current payload from spaceX's API to a file, to be consumed by notification-spacex.py
# Can be run on a timer, or you can just use Polybar's script module and add it to a bar, it doesn't return anything that will be displayed on Polybar.

import urllib.request as request
import json
from os.path import expanduser

home = expanduser("~")

json_filepath = home + "/.cache/spacex.json"

with request.urlopen('https://api.spacexdata.com/v4/launches/next') as url:
    json_payload = url.read().decode()

with open(json_filepath, "w+") as f:
    data = f.read()
    f.seek(0)
    f.write(json_payload)